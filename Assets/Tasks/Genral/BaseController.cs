﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseController : MonoBehaviour
{
    public int InitOrder;

    public virtual void Init()
    {

    }
}
