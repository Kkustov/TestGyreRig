﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
namespace MISC
{
    public static class Serializer
    {
        public static void Save<T>(T data, string path)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
            try
            {
                bf.Serialize(fs, data);
            }
            catch (System.Exception)
            {
                Debug.LogError($"{data.GetType()} incorrect format");
                throw;
            }
            fs.Close();
        }

        public static T Load<T>(string path)
        {
            if (!File.Exists(path))
                return default;

            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);
            var data = (T)bf.Deserialize(fs);
            fs.Close();
            return data;
        }

        public static void Remove(string path)
        {
            if (!File.Exists(path))
                return;
            File.Delete(path);
            Debug.Log($"Deleted: {path}");
        }
    }
}