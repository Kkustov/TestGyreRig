﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MISC;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;


namespace General
{

    public class DataManager
    {
        public static readonly string SavePath = $"{Application.persistentDataPath}/Data";

        List<IDataController> data_controllers;


        public DataManager(IList<BaseController> controllers)
        {
            data_controllers = new List<IDataController>();

            foreach (var ctrlr in controllers)
            {
                if (ctrlr is IDataController)
                    data_controllers.Add(ctrlr as IDataController);
            }
        }

        public Data Load(string name)
        {
            Debug.Log($"Load - Directory exist: {Directory.Exists(SavePath)}");
            if (!Directory.Exists(SavePath))
                Directory.CreateDirectory(SavePath);

            var loaded = Serializer.Load<Data>(GetDataPath(name));
            return loaded;
        }

        public void LoadAll()
        {
            Debug.Log($"Load - Directory exist: {Directory.Exists(SavePath)}");
            if (!Directory.Exists(SavePath))
                Directory.CreateDirectory(SavePath);
            for (int i = 0; i < data_controllers.Count; i++)
            {
                var loaded = Serializer.Load<Data>(GetDataPath(data_controllers[i]));
                Debug.Log($"Loaded (loaded!=nul): {GetDataPath(data_controllers[i])}");
                data_controllers[i].Load(loaded);
            }
        }

        public void SaveAll()
        {
            Debug.Log($"Save - Directory exist: {Directory.Exists(SavePath)}");
            if (!Directory.Exists(SavePath))
                Directory.CreateDirectory(SavePath);
            for (int i = 0; i < data_controllers.Count; i++)
            {
                var data = data_controllers[i].GetSave();
                if (data == null)
                {
                    Debug.LogError($"Empty data in {data_controllers[i].GetType()}!");
                    continue;
                }
                Serializer.Save(data, GetDataPath(data_controllers[i]));
            }
        }
        public void ClearAll()
        {
            Debug.Log("Clear All");
            Directory.Delete(SavePath, true);
            LoadAll();
        }

        public static void SimpleDeleteData()
        {
            Debug.Log("Delete Data Folder");
            if (Directory.Exists(SavePath))
            {
                Directory.Delete(SavePath, true);
                Debug.Log("Data deleted");
            }
            else
                Debug.Log("Data already empty");
        }


        static string GetDataPath(IDataController controller) => $"{SavePath}/{(controller as MonoBehaviour).gameObject.name}.data";
        static string GetDataPath(string name) => $"{SavePath}/{name}.data";

    }

    public interface IDataController
    {
        Data GetSave();
        void Load(Data baseData);
    }

    [Serializable]
    public abstract class Data
    {
        public Data()
        {

        }

    }
}