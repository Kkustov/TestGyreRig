﻿using General;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace FirstTask
{
    public class GameController : MonoBehaviour
    {

        private DataManager dataManager;
        private BaseController[] allControllers;

        private void Awake()
        {
            allControllers = FindObjectsOfType<BaseController>();
            dataManager = new DataManager(allControllers);
            LoadAll();
            InitAll();
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        private void LoadAll()
        {
            dataManager.LoadAll();
        }

        private void InitAll()
        {
            foreach (var item in allControllers.OrderBy(x => x.InitOrder).ToArray())
                item.Init();
        }

        private void OnApplicationQuit()
        {
            foreach (var item in allControllers)
                if (item is IGameExitListener)
                    (item as IGameExitListener).BeforeExit();
            dataManager.SaveAll();
        }
    }

    #region Interfaces
    /// <summary>
    /// Слушает событие выхода из игры
    /// </summary>
    public interface IGameExitListener
    {
        void BeforeExit();
    } 
    #endregion

}
