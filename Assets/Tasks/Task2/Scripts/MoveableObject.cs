﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using General;

namespace Task2
{
    public class MoveableObject : MonoBehaviour, IPoolReturner
    {
        #region Fields
        public Action ReturnInPool { get; set; }

        #endregion

        #region Main
        public void SetObject(float distance, float speed)
        {
            transform.position = Vector3.zero;
            float time = distance / speed;
            var tween = transform.DOLocalMoveZ(distance, time);
            tween.OnComplete(() => ReturnInPool());
        } 
        #endregion
    }
}

