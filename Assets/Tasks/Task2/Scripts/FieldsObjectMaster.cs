﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Task2
{
    public class FieldsObjectMaster : MonoBehaviour
    {
        #region Fields
        public Action<float> OnSpeedFieldChange, OnTimeFieldChange, OnDistanceFieldChange;


        [SerializeField] InputField speedField;
        [SerializeField] InputField timeField;
        [SerializeField] InputField distanceField;
        #endregion

        #region Init
        public void Init(float speed, float time, float distance)
        {
            speedField.onEndEdit.AddListener(AtSpeedFieldEndEdit);
            timeField.onEndEdit.AddListener(AtTimeFieldEndEdit);
            distanceField.onEndEdit.AddListener(AtDistanceFieldEndEdit);
            speedField.text = speed.ToString();
            timeField.text = time.ToString();
            distanceField.text = distance.ToString();
        }
        #endregion

        #region MISC
        void AtSpeedFieldEndEdit(string value)
        {
            if (float.TryParse(value, out var result))
            {
                OnSpeedFieldChange?.Invoke(result);
                return;
            }
            Debug.LogError($"Формат поля скорости неверен, полученное значение - {value}");
        }

        void AtTimeFieldEndEdit(string value)
        {
            if (float.TryParse(value, out var result))
            {
                OnTimeFieldChange?.Invoke(result);
                return;
            }
            Debug.LogError($"Формат поля Времени неверен, полученное значение - {value}");
        }

        void AtDistanceFieldEndEdit(string value)
        {
            if (float.TryParse(value, out var result))
            {
                OnDistanceFieldChange?.Invoke(result);
                return;
            }
            Debug.LogError($"Формат поля Расстояния неверен, полученное значение - {value}");
        }

        #endregion

    }
}