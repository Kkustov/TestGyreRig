﻿using General;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Task2
{
    public class MoveObjectController : BaseController, IDataController
    {
        #region Fields
        [SerializeField] FieldsObjectMaster fieldsObjectMaster;
        [SerializeField] SpawnObjectMaster spawnObjectMaster;


        MoveObjectData data;
        Tween tweenTimer; 
        #endregion

        #region Init

        public override void Init()
        {
            base.Init();
            fieldsObjectMaster.Init(data.SpeedMove, data.TimeSpawn, data.DistanceMove);
            spawnObjectMaster.Init();
            fieldsObjectMaster.OnDistanceFieldChange += AtChangeDistanceMove;
            fieldsObjectMaster.OnSpeedFieldChange += AtChangeSpeedMove;
            fieldsObjectMaster.OnTimeFieldChange += AtChangeTimeSpawn;
            SetTimer(data.TimeSpawn);
        }

        public Data GetSave() => data;

        public void Load(Data baseData)
        {
            data = baseData as MoveObjectData ?? new MoveObjectData();
        }
        #endregion

        #region MISC

        void AtChangeTimeSpawn(float time)
        {
            data.TimeSpawn = time;
            SetTimer(time);
        }

        void AtChangeSpeedMove(float speed)
        {
            data.SpeedMove = speed;
        }

        void AtChangeDistanceMove(float distance)
        {
            data.DistanceMove = distance;
        }

        void SetTimer(float time)
        {
            tweenTimer?.Kill();
            tweenTimer = DOVirtual.Float(time, 0, time, OnTimerTick).OnComplete(AtCompleteTimer).SetEase(Ease.Linear);
        }

        void OnTimerTick(float time)
        {

        }

        void AtCompleteTimer()
        {
            spawnObjectMaster.SpawnObject(data.DistanceMove, data.SpeedMove);
            tweenTimer?.Kill();
            tweenTimer = DOVirtual.Float(data.TimeSpawn, 0, data.TimeSpawn, OnTimerTick).OnComplete(AtCompleteTimer).SetEase(Ease.Linear);
        } 
        #endregion
    }
}

[Serializable]
public class MoveObjectData : Data
{
    [SerializeField] public float TimeSpawn { get; set; } = 1;
    [SerializeField] public float SpeedMove { get; set; } = 1;
    [SerializeField] public float DistanceMove { get; set; } = 1;

}
