﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;

namespace Task2
{
    public class SpawnObjectMaster : MonoBehaviour
    {
        #region Fields
        [SerializeField] MoveableObject prefab;


        SmartPool<MoveableObject> pool;
        #endregion

        #region Init
        public void Init()
        {
            pool = new SmartPool<MoveableObject>(prefab, transform);
        }
        #endregion

        #region Main
        public void SpawnObject(float distance, float speed)
        {
            var moveableObject = pool.Pop();
            moveableObject.SetObject(distance, speed);
        } 
        #endregion

    }
}