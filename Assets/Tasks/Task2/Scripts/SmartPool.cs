﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace General
{

    public class SmartPool<T> where T : Component
    {
        T prefab;
        Transform default_parent;
        List<SmartPullNode> list;

        #region Properties

        public event Action<T> OnItemCreated;
        /// <summary>
        /// Количество активных элементов в пуле, доступных для изъятия
        /// </summary>
        public int ActiveCount { get; private set; } = 0;
        /// <summary>
        /// Общее число элементов в пуле
        /// </summary>
        public int TotalCount => list.Count;
        /// <summary>
        /// Текущий родительский трансформ
        /// </summary>
        public Transform CurrentParent => default_parent;
        /// <summary>
        /// Текущи префаб для создания объектов
        /// </summary>
        public T CurrentPrefab => prefab;
        /// <summary>
        /// Scale = 1, LocalPosition = 0
        /// </summary>
        public bool UseDefaultTransformValidation { get; set; } = false;

        /// <summary>
        /// Method Chaining. <see cref="UseDefaultTransformValidation"/>
        /// </summary>
        public SmartPool<T> ActivateDefaultTransformValidation() { UseDefaultTransformValidation = true; return this; }

        #endregion

        #region Main

        /// <summary>
        /// Создает пулл объектов, взаимодействующий с объектами, расширяющими интерфейсы IPullReturner, IPullActivatable, IPullCreationable или без них.
        /// </summary>
        /// <param name="prefab">Объект, копии которого, будут заполнять пул</param>
        /// <param name="parent">Родитель, для всех объектов (при возвращении в пул объекты будут переносится под этот транстформ)</param>
        /// <param name="start_size">Начальный размер пула</param>
        public SmartPool(T prefab, Transform parent)
        {
            this.prefab = prefab;
            default_parent = parent;
            list = new List<SmartPullNode>();
        }

        public SmartPool(T prefab)
        {
            this.prefab = prefab;
            list = new List<SmartPullNode>();
        }

        /// <summary>
        /// Получить объект из пула (находится неиспользуемый или создается новый)
        /// </summary>
        public T Pop()
        {
            var result = list.FirstOrDefault(x => x.in_pool);
            result = result ?? ExtendPull();
            result.in_pool = false;
            ActiveCount--;
            if (result.value is IPoolBeforePopListener)
            {
                (result.value as IPoolBeforePopListener).BeforePopFromPool();
            }
            SetItemActive(result.value, true);
            if (result.value is IPoolPopListener)
            {
                (result.value as IPoolPopListener).OnPopFromPool();
            }
            return result.value;
        }

        public T Pop(Transform parent)
        {
            var poped = Pop();
            poped.transform.SetParent(parent);
            poped.transform.localScale = Vector3.one;
            return poped;
        }

        /// <summary>
        /// Получить объект из пула c пометкой о том, был ли он создан (true) или найден в пуле (falde)
        /// </summary>
        public bool Pop(out T result)
        {
            var node = list.FirstOrDefault<SmartPullNode>(x => x.in_pool);
            bool is_new = node == null;
            node = node ?? ExtendPull();
            node.in_pool = false;
            ActiveCount--;
            SetItemActive(node.value, true);
            if (node.value is IPoolPopListener)
            {
                (node.value as IPoolPopListener).OnPopFromPool();
            }
            result = node.value;
            return is_new;
        }

        public List<T> Pop(int count, Action<T> initValidation = null)
        {
            List<T> result = new List<T>();
            for (int i = 0; i < count; i++)
            {
                if (Pop(out var poped) && initValidation != null)
                    initValidation(poped);
                result.Add(poped);
            }
            return result;
        }

        /// <summary>
        /// Возвращает элемент в пул
        /// Для объектов с интерфейсом IPullReturner этот метод прокидывается в поле ReturnInPull
        /// </summary>
        /// <param name="item"></param>
        public void ReturnItemInPull(T item)
        {
            var node = list.FirstOrDefault(x => x.value == item && !x.in_pool);
            if (node == null)
                return;

            if (item is IPoolReturnListener)
                ((IPoolReturnListener)item).BeforeReturnInPool();

            node.in_pool = true;
            SetItemActive(item, false);
            if (default_parent != null)
                item.transform.SetParent(default_parent);
            ActiveCount++;
        }

        public void ReturnAllItems()
        {
            var sorted_arry = list.Where(x => !x.in_pool);

            foreach (var item in sorted_arry)
            {
                ReturnItemInPull(item.value);
            }
            //list.Where(x => !x.in_pool).ToList().ForEach(x => ReturnItemInPull(x.value));
        }

        public List<T> ActiveElements => list.Where(x => !x.in_pool).Select<SmartPullNode, T>(x => x.value).ToList();

        #endregion

        #region Расширение пула

        /// <summary>
        /// Создание и регистрация одной ноды
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        List<SmartPullNode> ExtendPull(int size)
        {
            List<SmartPullNode> new_items = new List<SmartPullNode>();
            for (int i = 0; i < size; i++)
                new_items.Add(ExtendPull());
            return new_items;
        }

        /// <summary>
        /// Создание и регистрация нескольких нод
        /// </summary>
        /// <returns></returns>
        SmartPullNode ExtendPull()
        {
            T item = prefab is IPoolCreationable creationable ? (T)creationable.Create() : CreateDefault();
            SetItemActive(item, false);
            if (item is IPoolReturner returner)
                returner.ReturnInPool = () => { ReturnItemInPull(item); };
            SmartPullNode result = new SmartPullNode(item, true);
            list.Add(result);
            ActiveCount++;
            OnItemCreated?.Invoke(item);
            return result;
        }

        /// <summary>
        /// Создание объекта не расширяющего интерфейс IPullCreationable
        /// </summary>
        /// <returns></returns>
        T CreateDefault()
        {
            var new_object = GameObject.Instantiate<T>(prefab);
            new_object.transform.SetParent(default_parent);
            if (UseDefaultTransformValidation)
            {
                new_object.transform.localPosition = Vector3.zero;
                new_object.transform.localScale = Vector3.one;

            }
            return new_object;
        }

        /// <summary>
        /// Удаляет все содержащиеся MonoBehaviour и чистит пул
        /// </summary>
        public void ClearPool()
        {
            if (list.Count == 0 || !(list[0] is MonoBehaviour))
                return;
            var cnt = list.Count;
            for (int i = 0; i < cnt; i++)
                GameObject.Destroy((list[i] as MonoBehaviour).gameObject);
            list.Clear();
        }

        #endregion

        #region AUX
        /// <summary>
        /// Включает/выключает объект для выхода/входа входа в пул соответственно
        /// </summary>
        /// <param name="item"></param>
        /// <param name="active"></param>
        void SetItemActive(T item, bool active)
        {
            if (item is IPoolActivatable activable)
                activable.SetActiveForPool(active);
            else
                item.gameObject.SetActive(active);
        }

        /// <summary>
        /// Нода с информацией о том, что объект изъят из пула
        /// </summary>
        /// <typeparam name="T"></typeparam>
        class SmartPullNode
        {
            public T value;
            public bool in_pool;

            public SmartPullNode(T value, bool in_pool)
            {
                this.value = value;
                this.in_pool = in_pool;
            }
        }
        #endregion
    }

    /// <summary>
    /// Позволяет хранить в объекте делегат, возвращающий объект в пул
    /// Делегат будет создаваться SmartPull'ом и помещаться в поле ReturnInPool автоматически для всех IPoolReturner объектов в пуле
    /// </summary>
    public interface IPoolReturner
    {
        Action ReturnInPool { get; set; }
    }

    /// <summary>
    /// Позволяет переопределить скрытие пулом (по умолчанию используется gameObject.SetActive)
    /// </summary>
    public interface IPoolActivatable
    {
        void SetActiveForPool(bool active);
        bool IsActiveForPool { get; }
    }

    /// <summary>
    /// Дефолтная инстанциация префаба при расширении пула заметяется на результат  функции Create
    /// </summary>
    public interface IPoolCreationable
    {
        Component Create();
    }

    /// <summary>
    /// Метод OnReturnInPool вызывается перед возвращением в пул
    /// </summary>
    public interface IPoolReturnListener
    {
        void BeforeReturnInPool();
    }
    /// <summary>
    /// Метод BeforePopFromPool вызывается перед выходом из пула
    /// </summary>
    public interface IPoolBeforePopListener
    {
        void BeforePopFromPool();
    }

    /// <summary>
    /// Метод OnReturnInPool вызывается перед возвращением в пул
    /// </summary>
    public interface IPoolPopListener
    {
        void OnPopFromPool();
    }
}