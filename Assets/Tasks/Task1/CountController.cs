﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;
using System;

public class CountController : BaseController, IDataController
{
    #region Fields
    [SerializeField] CountMaster countMaster;

    CountData data;
    #endregion


    #region Init

    public override void Init()
    {
        countMaster.Init(data.Count);
        countMaster.OnClickButton += AtClickCountButton;
    }

    Data IDataController.GetSave() => data;

    void IDataController.Load(Data baseData)
    {
        data = baseData as CountData ?? new CountData();
    }
    #endregion

    #region MISC

    private void AtClickCountButton()
    {
        AddCount();
    }

    private void AddCount()
    {
        data.Count++;
        countMaster.SetCount(data.Count);
    } 
    #endregion

}

[Serializable]
public class CountData : Data
{
    [SerializeField] public int Count { get; set; } = 0;

}

