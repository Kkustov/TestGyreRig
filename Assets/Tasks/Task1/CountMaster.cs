﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountMaster : MonoBehaviour
{
    #region Fields
    readonly string countLabel = "Your count is: \n";
    public event Action OnClickButton;

    [SerializeField] Button countButton;
    [SerializeField] Text countText;

    #endregion

    #region Main
    public void Init(int count)
    {
        countButton.onClick.AddListener(AtButtonClick);
        SetCount(count);
    }


    public void SetCount(int count)
    {
        countText.text = $"{countLabel} {count}";
    }

    #endregion


    #region MISC
    private void AtButtonClick()
    {
        OnClickButton?.Invoke();
    } 
    #endregion

}
