﻿


public static class ObjectAExtension
{
    public static void SetupObjectA(this ApiSetup<ObjectA> str)
    {
    }
}

public static class ObjectBExtension
{
    public static void SetupObjectB(this ApiSetup<ObjectB> str)
    {
    }
}


public struct ApiSetup<T>
{

}

class Api
{
    public ApiSetup<T> For<T>(T obj)
    {
        return new ApiSetup<T>();
    }
}
interface ISomeInterfaceA
{

}
interface ISomeInterfaceB
{

}
public class ObjectA : ISomeInterfaceA
{

}
public class ObjectB : ISomeInterfaceB
{

}
class SomeClass
{
    public void Setup()
    {
        Api apiObject = new Api();

        apiObject.For(new ObjectA()).SetupObjectA();
        apiObject.For(new ObjectB()).SetupObjectB();

    }
}