﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Task3 : MonoBehaviour
{
    class SomeClass
    {

        public void Run()
        {
            var someStruct = new SomeStruct();
            SomeMethod(someStruct);
        }

        void SomeMethod<T>(T @interface) where T : ISomeInterface<T>
        {
            @interface.Call();
        }
    }


    interface ISomeInterface<T>
    {
        void Call();
    }

    struct SomeStruct : ISomeInterface<SomeStruct>
    {
        public void Call()
        {

        }
    }

}
